describe('Todo testing', ()=>{
    context('Todo input',()=>{
        beforeEach(()=>{
            cy.visit('/')
        });
        it('Can open todo site',()=>{
            
        })
        it('can add new todo via enter', ()=>{
            const text = 'buy goods'
            cy.get('[data-automationid="addTodoInput"]')
            .type(text)
            .type('{enter}')
            cy.get('[data-automationid="todoText"]').should('contain',text)
        })

        it('can add new todo via add button', ()=>{
            const text = 'buy shop'
            cy.get('[data-automationid="addTodoInput"]')
            .type(text)
            cy.get('[data-automationid="addTodoButton"]').click()
            cy.get('[data-automationid="todoText"]').should('contain',text)
        })
    })

    context('Todo remove', ()=>{
        it('can remove todo',()=>{
            const text = 'buy house'
            cy.get('[data-automationid="addTodoInput"]')
            .type(text)
            .type('{enter}')
            cy.get('[data-automationid="todoText"]').should('contain',text)
            cy.get(`[data-automationId="delete:${text}"]`).click()
            cy.contains(text).should('not.exist')
        })
    })
})