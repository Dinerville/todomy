const initialState = [
    {
        id: 0,
        text: 'buy eggs',
        isCompleted: false
    },
    {
        id: 1,
        text: 'buy bread',
        isCompleted: false
    },
    {
        id: 2,
        text: 'buy bacon',
        isCompleted: false
    },
]

export default function todos(state = initialState, action){
    switch(action.type){
        case 'ADD_TODO':
            return [...state, action.payload];
        case 'REMOVE_TODO':
            return state.filter(todo => todo.id !== action.payload)
        case 'COMPLETE_TODO':
            return state.map((todo) => {
                if(todo.id === action.payload){
                    todo.isCompleted = !todo.isCompleted
                }
                return todo;
            });
        default:
            return state;
    }
}