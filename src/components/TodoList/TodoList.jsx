import React from 'react';
import { connect} from 'react-redux';
import './TodoList.css';

class TodoList extends React.Component{

    deleteTodo(event){
        var attribute = event.target.attributes.getNamedItem('data-id').value;
        this.props.onRemoveTodo(parseInt(attribute));
    }

    markAsChecked(event){
        var attribute = event.target.attributes.getNamedItem('data-id').value;
        this.props.onCompleteTodo(parseInt(attribute));
    }

    render(){
        return(
            <div>
                {this.props.todos.map(todo => (
                    <div data-automationId={`todo:${todo.text}`}>
                        <input type='checkbox' data-automationId='isCompleted' data-id={todo.id} checked={todo.isCompleted} onChange={this.markAsChecked.bind(this)}/>
                        <span data-automationId='todoText'>{todo.text}</span>
                        <button className='delete' data-automationId={`delete:${todo.text}`} data-id={todo.id} key={todo.id} onClick={this.deleteTodo.bind(this)}>X</button>
                    </div>
                ))}
            </div>
        );
    }
}

export default connect(
    state=>({
        todos: state.todos
    }),
    dispatch => ({
        onRemoveTodo: (todoId) =>{
            dispatch({type: 'REMOVE_TODO',payload: todoId})
        },
        onCompleteTodo:(todoId) => {
            dispatch({type: 'COMPLETE_TODO', payload: todoId})
        }
    })
)(TodoList);