import React from 'react';
import { connect} from 'react-redux';

class TodoAdd extends React.Component{

    addTodo(event){
        event.preventDefault();

        var payload = {
            id: parseInt(Date.now().toString()),
            text: this.refs.todoInput.value,
            isCompleted:false
        }
        this.props.onAddTodo(payload);
        this.refs.todoInput.value = ''
    }

    render(){
        return(
            <div>
                <form onSubmit={this.addTodo.bind(this)}>
                    <input type='text' data-automationId='addTodoInput' ref='todoInput' placeholder='enter your todo' />
                </form>
                <button onClick={this.addTodo.bind(this)} data-automationId='addTodoButton'>Add Todo</button>
            </div>
        );
    }
}

export default connect(
    state=>({
        
    }),
    dispatch => ({
        onAddTodo: (todo) =>{
            dispatch({type: 'ADD_TODO',payload: todo})
        }
    })
)(TodoAdd);