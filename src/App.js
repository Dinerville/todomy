import React from 'react';
import './App.css';
import TodoList from './components/TodoList/TodoList'
import TodoAdd from './components/TodoAdd/TodoAdd'

class App extends React.Component {
  render(){
  return (
    <div className="App">
      <TodoAdd/>
      <TodoList/>
    </div>
  );
}
}

export default App;
